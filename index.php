<?php

require 'vendor/autoload.php';
// import dbconn
require 'dbconn.php';
$router = new \Bramus\Router\Router();

// routes

$router->get('/feature', function() use ($conn) {
    if (isset($_GET['email']) && isset($_GET['feature'])) {
        // sanitize inputs for sql 
        $email = mysqli_real_escape_string($conn, $_GET['email']);
        $feature = mysqli_real_escape_string($conn, $_GET['feature']);

        // check if email exists
        $sql = "SELECT * FROM permissions WHERE email = '$email' AND feature = '$feature' AND enabled = 1";
        $result = @mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            http_response_code(200);
            echo json_encode(array("canAccess" => true));
        } else {
            http_response_code(200);
            echo json_encode(array("canAccess" => false));
        }
    } else {
        http_response_code(400);
        echo json_encode(array("message" => "Bad Request"));
    }
});
$router->post('/feature', function() use ($conn){
    // get json data
    $data = json_decode(file_get_contents("php://input"));
    // check if data are set
    if (isset($data->email) && isset($data->feature) && isset($data->enable)) {
        // validate email
        if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo json_encode(array("message" => "Bad Email"));
            die();
        }
        $email = mysqli_real_escape_string($conn, $data->email);
        $feature = mysqli_real_escape_string($conn, $data->feature);
        // enabled is a boolean, change to int
        $enabled = $data->enable ? 1 : 0;

        // check if email exists
        $sql = "SELECT * FROM permissions WHERE email = '$email' AND feature = '$feature'";
        $result = @mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            // update
            $row = mysqli_fetch_assoc($result);
            $status = $row['enabled'];
            if ($status != $enabled){
                $sql = "UPDATE permissions SET enabled = '$enabled' WHERE email = '$email' AND feature = '$feature'";
                if (@mysqli_query($conn, $sql)) {
                    http_response_code(200);
                } else {
                    http_response_code(500);
                    echo json_encode(array("message" => "Internal Server Error"));
                }
            }
            else {
                http_response_code(304);
                echo json_encode(array("message" => "Not Modified"));
            }
        } else {
            // insert
            $sql = "INSERT INTO permissions (email, feature, enabled) VALUES ('$email', '$feature', '$enabled')";
            if (@mysqli_query($conn, $sql)) {
                http_response_code(200);
            } else {
                http_response_code(500);
                echo json_encode(array("message" => "Internal Server Error"));
            }
        }
    } else {
        http_response_code(400);
        echo json_encode(array("message" => "Bad Request"));
    }
});
// end routes

$router->run();
mysqli_close($conn);
?>