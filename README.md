# Moneylion Take-home assignment

## Description
This repository is made as a submission for the take-home assignment by Moneylion. The assignment consists of an API Endpoint, with both GET and POST methods available.

### GET /feature?email=***&feature=\*\*\*
This endpoint receives 2 parameters (`email` and `feature`) and checks against a database whether that the particular email has access to a feature. It will return a json key "canAccess" with value of either True or False.

### POST /feature
This endpoint receives a JSON payload that contains 3 keys (`email`, `feature` and `enable`) which will update (or create) an entry based on the users' access permissions. Explicitly mentioned in the assignment, if a value is not updated, then it must return a status code of Not Modified (304). 

## Requirements
- PHP 7 and above is known to work well.
- MySQL 8
- Composer

## Installation
1. Clone the repository
2. copy .env.example and rename to .env
3. edit environment variables for database connection.
4. run `composer install`
5. In MySQL Server, import the `permissions.sql` file to create the table.
6. For testing, run `php -S localhost:8000` to run the service locally on port 8000
7. For production use, place the file in htdocs or use vHosts.

## Usage

### GET /feature
Make a GET request with URL parameters `email` and `feature` set to the values you want to check.

***Example***

Database:
```
+---------------+---------+---------+
| email         | feature | enabled |
+---------------+---------+---------+
| test@test.com | test    |       1 |
+---------------+---------+---------+
```

Request URI:
`http://localhost:9090/feature?email=test@test.com&feature=test`

Response:
`{"canAccess":true}`

### POST /feature

Make a POST request with a JSON data as the request body with 3 keys (`email`, `feature` and `enable`) set.

***Example***

JSON Body:
```
{
  "email" : "test@test.com",
  "feature" : "test",
  "enable" : false
}
```

Response: `Status Code 200 (Empty Response)`

Database (After POST):
```
+---------------+---------+---------+
| email         | feature | enabled |
+---------------+---------+---------+
| test@test.com | test    |       0 |
+---------------+---------+---------+
```
