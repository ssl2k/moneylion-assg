<?php
require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$host = $_ENV['DBHOST'] . ':' . $_ENV['DBPORT'];
$user = $_ENV['DBUSER'];
$pass = $_ENV['DBPASS'];
$db = $_ENV['DBNAME'];
$conn = mysqli_connect($host, $user, $pass, $db);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>